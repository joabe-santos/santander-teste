import React from 'react';

import Routes from './Routes';

class Root extends React.Component {
    render() {
        return (
            <Routes />
        )
    }
}

export default Root;
