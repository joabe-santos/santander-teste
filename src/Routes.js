import { createStackNavigator } from 'react-navigation';

// my routes
import CreditScreen from './modules/add-credit/CreditScreen';
import ConfirmScreen from './modules/add-credit/ConfirmScreen';


const Routes = createStackNavigator({
    Home: { screen: CreditScreen },
    ConfirmScreen: { screen: ConfirmScreen }
},
{
    navigationOptions: { header: null }
});

export default Routes;
