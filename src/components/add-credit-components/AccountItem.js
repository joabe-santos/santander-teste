import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import Swiper from 'react-native-swiper';

import { colors, icons } from '../../utils';

const AccountItem = ({ data }) => (
    <View>
        <View style={styles.main}>
            <TouchableOpacity onPress={() => this.swiper.scrollBy(-1, true)} style={styles.arrow}>
                <Image style={styles.imgSt} source={icons['left']} />
            </TouchableOpacity>
            <Swiper 
                ref={swiper => (this.swiper = swiper)}
                activeDotColor={colors.red}
            >
                {data.map((cnt, ix) => ( 
                    <View key={ix}>
                        <View>
                            <Text style={styles.txtOne}>{cnt.cntName} <Text style={styles.txtTwo}>{cnt.cntNunber}</Text></Text>
                            <View style={styles.bodySwiper}>
                                <Text style={styles.txtItems}>Saldo disponível:</Text>
                                <Text style={styles.txtItems}>{cnt.cntCredit}</Text>
                                <Text style={styles.txtItems}>Saldo disponível total:</Text>
                                <Text style={styles.txtItems}>{cnt.totalCredit}</Text>
                            </View>
                        </View>
                    </View>
                ))}
            </Swiper>
            <TouchableOpacity onPress={() => this.swiper.scrollBy(1, true)} style={styles.arrow}>
                <Image style={styles.imgSt} source={icons['right']} />
            </TouchableOpacity>
        </View>
        <View style={styles.ln} />
    </View>
);

const styles = {
    main: {
        flexDirection: 'row',
        height: 140,
    },
    imgSt: {
        width: 20,
        height: 20
    },
    arrow: {
        marginTop: 50
    },
    txtOne: {
        marginTop: 6, 
        fontWeight: 'bold', 
        alignSelf: 'center'
    },
    txtTwo: {
        fontWeight: 'normal'
    },
    txtItems: {
        color: colors.mediuGray
    },
    bodySwiper: {
        marginBottom: 5, 
        marginRight: '15%', 
        marginLeft: '15%', 
        alignItems: 'center', 
        backgroundColor: colors.lightGray
    },
    ln: {
        width: '100%',
        borderColor: colors.lightGray,
        borderWidth: 1,
        position: 'absolute',
        bottom: 40
    }
}

export default AccountItem;
