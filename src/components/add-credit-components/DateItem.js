import React from 'react';
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native';
import Swiper from 'react-native-swiper';

import { colors, icons } from '../../utils';

class DateItem extends React.Component {
    state = {
        index: 0,
        ixValue: 0
    };

    setupIndex = (it) => {
        this.setState({ ixValue: it });
        this.props.pressSelectDate(it);
    };

    render() {
        const { data } = this.props;
        return (
            <View>
                <View style={{ height: 100, marginTop: 20, flexDirection: 'row' }}>
                    <TouchableOpacity onPress={() => {this.state.ixValue >= 1 ? this.swiper.scrollBy(-1, true) : {}}}>
                        <Image style={styles.imgSt} source={icons['left']} />
                    </TouchableOpacity>
                    <Swiper 
                        ref={swiper => (this.swiper = swiper)}
                        onIndexChanged={index => this.setupIndex(index)}
                        activeDotColor={colors.red}
                        loop={false}
                    >
                        {data.map((it, ix) => (
                            <View key={ix}>
                                <TouchableOpacity onPress={it.pressPicker ? it.pressPicker : null}  style={styles.bodySwiper}>
                                    <Text style={{ fontWeight: 'bold', color: colors.mediuGray, fontSize: 16 }}>{it.day}</Text>
                                    <Text style={{ color: colors.lightGray, fontSize: 16 }}>{it.date}</Text>
                                </TouchableOpacity>
                            </View>
                        ))}
                    </Swiper>
                    <TouchableOpacity onPress={() => { this.state.ixValue < 1 ? this.swiper.scrollBy(1, true) : {} }}>
                        <Image style={styles.imgSt} source={icons['right']} />
                    </TouchableOpacity>
                </View>
                <View style={styles.ln} />
            </View>
        );
    }
    
}

const styles = {
    main: {
        height: 80,
        marginTop: 20,
        flexDirection: 'row'
    },
    imgSt: {
        width: 20,
        height: 20
    },
    bodySwiper: {
        marginBottom: 10, 
        marginRight: '15%', 
        marginLeft: '15%', 
        alignItems: 'center'
    },
    ln: {
        width: '100%',
        borderColor: colors.lightGray,
        borderWidth: 1,
        position: 'absolute',
        bottom: 40
    }
}

export default DateItem;
