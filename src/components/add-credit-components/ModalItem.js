import React from 'react';
import { View, Text, Modal, TouchableOpacity, ScrollView } from 'react-native';
import { Container, Input } from '../forms';

import { colors } from '../../utils';

const ModalItem = ({ data, dataSeached, modalVisible, closeModal, handleModal, pressSelect, searchTxt, searchItem }) => (
    
    <TouchableOpacity onPress={handleModal} style={styles.main}>
        <View>
            <Text style={styles.txtFront}>Selecione a operadora</Text>
        </View>
        {modalVisible && 
        <Modal 
            transparent
            visible={modalVisible} 
            onRequestClose={closeModal}
        >
            <Container style={styles.wrapper}>
                <View style={styles.body}>
                    <Text style={styles.txtTitle}>Selecione a operadora</Text>
                    <View style={styles.line} />
                    <Input
                        search
                        long={100}
                        value={searchTxt}
                        editable
                        autoCorrect={false}
                        onChangeText={value => searchItem(value)}
                        multiline={false}
                        underlineColorAndroid='transparent'
                        placeholder='Busque aqui uma operadora'
                    />
                    <ScrollView>
                        {dataSeached.length > 0 ? dataSeached.map((it, i) => (
                            <TouchableOpacity onPress={() => pressSelect(it)} key={i} style={styles.opItem}>
                                <Text style={styles.txtItem}>{it}</Text>
                            </TouchableOpacity>
                        )) : data.map((op, ix) => (
                            <TouchableOpacity onPress={() => pressSelect(op)} key={ix} style={styles.opItem}>
                                <Text style={styles.txtItem}>{op}</Text>
                            </TouchableOpacity>
                        ))}
                       
                    </ScrollView>
                </View>
            </Container>
        </Modal>}
    </TouchableOpacity>
);

const styles = {
    main: {
        backgroundColor: colors.mediuGray,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center'
    },
    wrapper: {
        backgroundColor: 'rgba(0, 0, 0, 0.5)'
    },
    body: {
        backgroundColor: colors.white, 
        position: 'absolute', 
        left: 40, 
        right: 40,  
        top: 20, 
        bottom: 20
    },
    line: {
        height: 3,
        backgroundColor: colors.red
    },
    txtTitle: {
        padding: 20, 
        fontWeight: 'bold', 
        fontSize: 18, 
        color: colors.red, 
        alignSelf: 'center'
    },
    txtItem: {
        color: colors.gray
    },
    txtFront: {
        color: colors.white
    },
    opItem: {
        padding: 16, 
        borderBottomWidth: 1, 
        borderBottomColor: colors.gray
    }
}

export default ModalItem;
