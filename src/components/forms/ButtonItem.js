import React from 'react';
import { TouchableOpacity, Text } from 'react-native';
import { colors } from '../../utils';

const ButtonItem = ({ title, widthValue, press }) => (
    <TouchableOpacity onPress={press} style={[styles.main, { width: `${widthValue}%` }]}>
        <Text style={{ color: colors.white }}>{title}</Text>
    </TouchableOpacity>
);

const styles = {
    main: {
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        height: 40,
        marginBottom: 20,
        backgroundColor: colors.mediuGray
    }
}

export default ButtonItem;
