import React from 'react';
import { TouchableOpacity, Image } from 'react-native';
import { Text, View } from 'react-native';

import { colors, icons } from '../../utils/';

const CheckBox = ({ value, press, label }) => (
    <View style={{ alignSelf: 'center' }}>
        <TouchableOpacity style={styles.containerTouch} onPress={press}>
            <Text style={styles.txtlabel}>{label}</Text>
            <View style={styles.check}>
                {value && <Image source={icons['ckeck']} style={styles.checked} />}
            </View>
        </TouchableOpacity>
    </View>
)


const styles = {
    containerTouch: {
        flexDirection: 'row'
    },
    check: {
        height: 18,
        width: 18,
        borderWidth: 1,
        borderColor: colors.lightGray,
    },
    checked: {
        height: 18,
        width: 18,
    },
    txtlabel: {
        color: colors.lightGray,
        fontSize: 14,
        marginRight: 10
    },
}

export default CheckBox;