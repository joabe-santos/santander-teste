import styled from 'styled-components';
import { SafeAreaView } from 'react-native';

import { colors } from '../../utils';

export default styled(SafeAreaView)`
  flex: 1;
  background-color: ${colors.white};
`;
