import styled from 'styled-components';

import { colors } from '../../utils';

export default styled.ScrollView`
    flex: 1;
    background-color: ${colors.white};
    padding-left: 16px;
    padding-right: 16px;
`;
