import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';

import { colors } from '../../utils';
const ic = require('../../assets/menu-lines.png')

const Header = ({ title }) => (
    <View style={styles.main}>
        <TouchableOpacity style={{ marginLeft: 16 }}>
            <Image style={{ width: 26, height: 22 }} source={ic} />
            <Text style={{ color: colors.white, fontSize: 11 }}>MENU</Text>
        </TouchableOpacity>
        <View style={{ alignItems: 'center', justifyContent: 'center', height: 60, flex: 1, marginRight: 45 }}>
            <Text style={styles.titleSt}>{title}</Text>
        </View>
    </View>
);

const styles = {
    main: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: colors.red,
        height: 60,
    },
    titleSt: {
        fontSize: 18,
        color: colors.white
    }
}

export default Header;
