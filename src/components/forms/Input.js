import React from 'react';
import { View, TextInput  } from 'react-native';

import { colors } from '../../utils';

const InputText = ({ icon, long, press, search, ...props }) => (
    <View style={[styles.main, { width: `${long}%`, marginRight: 6 }]}>
        <TextInput style={styles.txtInput} {...props} />
        <View style={{ flexDirection: 'row' }}>
            <View style={[search ? styles.leftRed : styles.baseLeft]} />
            <View style={[search ? styles.baseRed : styles.baseInput]} />
            <View style={[search ? styles.rightRed : styles.baseRight]} />
        </View>
    </View>
);

const styles = {
    main: {
        height: 50,
        width: '100%',
    },
    txtInput: {
        left: 8,
        flex: 1,
        fontSize: 18,
        color: colors.lightGray
    },
    baseInput: {
        flex: 1,
        borderWidth: 1,
        height: 1,
        borderColor: colors.lightGray,
        alignSelf: 'flex-end'
    },
    baseRed: {
        flex: 1,
        borderWidth: 1,
        height: 1,
        borderColor: colors.red,
        alignSelf: 'flex-end'
    },
    baseLeft: {
        height: 6,
        borderWidth: 1,
        borderColor: colors.lightGray,
    },
    leftRed: {
        height: 6,
        borderWidth: 1,
        borderColor: colors.red,
    },
    baseRight: {
        height: 6,
        borderWidth: 1,
        borderColor: colors.lightGray,
    },
    rightRed: {
        height: 6,
        borderWidth: 1,
        borderColor: colors.red,
    }
}

export default InputText;