import React from 'react';
import { View } from 'react-native';

import { colors } from '../../utils';

const Line = () => (
    <View style={styles.main} />
);

const styles = {
    main: {
        borderWidth: 1,
        height: 1,
        width: '100%',
        borderColor: colors.lightGray
    }
};

export default Line;
