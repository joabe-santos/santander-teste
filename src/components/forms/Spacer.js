import styled from 'styled-components'

export default styled.View`
  height: ${props => props.height}px;
`;