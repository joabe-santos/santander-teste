import React, {Component} from 'react';
import { Text, View, StatusBar, Platform  } from 'react-native';

import { Container, Content, Line, Spacer, ButtonItem, Header } from '../../components/forms';
import { colors } from '../../utils';

class ConfirmScreen extends Component {
  render() {
    const { data } = this.props.navigation.state.params;
    return (
      <Container style={styles.container}>
        <StatusBar barStyle={Platform.OS === 'ios' ? 'dark-content' : 'light-content'} backgroundColor={colors.black} />
        <Header 
          title='Recarga'
        />
        <Text style={styles.txtTopic}>CONFIRMAÇÂO</Text>
        <Line />
        <Content>
            <Spacer height={20} />
            <View>
              <Text style={styles.stTitle}>Operadora: <Text style={styles.stItem}>{data.operadora}</Text></Text>
              <Text style={styles.stTitle}>N° de telefone: <Text style={styles.stItem}>{data.fone}</Text></Text>
              <Text style={styles.stTitle}>Valor desejado: <Text style={styles.stItem}>{data.valor}</Text></Text>
            </View>
            <Spacer height={40} />
            <Text>A efetivação da transação está sujeito a disponibilidade do saldo no dia</Text>
        </Content>
          <ButtonItem 
            press={() => {}}
            title='Confirmar'
            widthValue={50}
          />
      </Container>
    );
  }
}

const styles = {
  txtTopic: {
    marginLeft: 16,
    marginTop: 20,
    marginBottom: 5,
    color: colors.black
  },
  stTitle: {
    color: colors.darkGray
  },
  stItem: {
    color: colors.lightGray
  }
};

export default ConfirmScreen;
