import React from 'react';
import { Text, View, StatusBar, ToastAndroid, TouchableOpacity, Image, Platform } from 'react-native';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { selectContactPhone } from 'react-native-select-contact';
import moment from 'moment';

import { Container, Content, Header, Line, Spacer, CheckBox, Input, ButtonItem } from '../../components/forms';
import { AccountItem, ModalItem, ValuesItem, DateItem } from '../../components/add-credit-components'
import { colors, icons, months } from '../../utils';

const atualDate = moment().format('DD/MM/YYYY');

export default class CreditScreen extends React.Component {
    
    state = {
        pickerVisible: false,
        fddd: false,
        fcddd: false,
        fnumber: false,
        fcnumber: false,
        modal: false,
        saveRec: false,
        opSelected: '',
        dateSelected: atualDate,
        valueSelected: 'R$ 15,00',
        txtSearch: '',
        ddd: '',
        number: '',
        dddConfirm: '',
        numberConfirm: '',
        searchedList: [],
    };

    getPhoneNumber = () => {
        return selectContactPhone()
            .then(selection => {
                if (!selection) {
                    return null;
                }
                const { selectedPhone } = selection;
                const item = selectedPhone.number.split("");
                const rest = (item.length) - (11);
                const num = selectedPhone.number.slice(rest, item.length);
                const ddd = num.slice(0, 2);
                const number = num.slice(2, 11);
                this.setState({ ddd, number, dddConfirm: ddd, numberConfirm: number });
            });  
    };

    _searchItem = value => {
        this.setState({ txtSearch: value });
        let searched = [];
        for (const item of this.listOp()) {
            if (value === '') {
                searched = [];
            } else {
                if (item.toLowerCase().indexOf(value) !== -1 || item.indexOf(value) !== -1) {
                    searched.push(item);
                }
            }
        }
        setTimeout(() => {
            this.setState({ searchedList: searched }); 
        }, 20);
    };

    listAccounts = () => [
        {
            cntName: 'Conta Santander',
            cntNunber: '0611/01.833345.7',
            cntCredit: '3.420,44',
            totalCredit: '3.420,44'
        },
        {
            cntName: 'Conta Santander',
            cntNunber: '0311/02.833345.7',
            cntCredit: '7.420,44',
            totalCredit: '7.420,44'
        }
    ];

    listOp = () => [
        'CLARO BA / SE',
        'CLARO CO',
        'CLARO MG',
        'CLARO NE',
        'CLARO NO DDD 91 A 99',
        'CLARO PR / SC',
        'CLARO RJ / ES',
        'CLARO RS',
        'CLARO SP DDD 11',
        'VIVO SP 11',
        'VIVO RJ',
        'VIVO MG',
        'TIM SP 11',
        'TIM RJ',
        'TIM MG',
        'NEXTEL',
    ];

    listValues = () => [
        'R$ 15,00',
        'R$ 20,00',
        'R$ 25,00',
        'R$ 30,00',
        'R$ 35,00',
        'R$ 40,00',
        'R$ 45,00',
    ];

    listDate = () => [
        {day: 'RECARREGAR HOJE', date: `${atualDate}`},
        {day: 'AGENDAR', date:`${this.state.dateSelected}`, pressPicker: () => this._showPicker() }
    ];

    setSelected = value => {
        this.setState({ opSelected: value, modal: false, searchedList: [] });
    };

    setDate = value => {
        const item = this.listDate();
        this.setState({ dateSelected: item[value] });
    };

    setValue = value => {
        const item = this.listValues();
        this.setState({ valueSelected: item[value] });
    };

    _showPicker = () => this.setState({ pickerVisible: true });

    _hideDateTimePicker = () => this.setState({ pickerVisible: false });

    _handleDatePicked = (valuePicker) => {
        const day = `${valuePicker}`;
        dia = day.slice(8, 10);
        mes = day.slice(4, 7);
        m = months(mes);
        ano = day.slice(11, 15);
        this.setState({ dateSelected: `${dia}/${m}/${ano}` })
        this._hideDateTimePicker();
    };

    setupAlert = (message) => {
        alert(message);
    };

    handleRequest = () => {
        const { ddd, dddConfirm, number, numberConfirm, opSelected, valueSelected } = this.state;
        const num = `(${ddd}) ${number}`;
        const confNum = `(${dddConfirm}) ${numberConfirm}`;
        if (num !== confNum) {
            this.setupAlert('Os números não são iguais');
        } else if (opSelected === '') {
            this.setupAlert('Selecione a operadora');
        } else if (num === '' || ddd === '') {
            this.setupAlert('Campo de número vazio');
        } else {
            const data = { operadora: opSelected, fone: `(${ddd}) ${number}`, valor: valueSelected };
            this.props.navigation.navigate('ConfirmScreen', { data });
        }
    };

    render() {
        const { 
            ddd, 
            number,
            dddConfirm,
            numberConfirm,
            opSelected, 
            txtSearch, 
            searchedList,
            focus,
            modal } = this.state;
        return (
            <Container>
                <StatusBar barStyle={Platform.OS === 'ios' ? 'dark-content' : 'light-content'} backgroundColor={colors.black} />
                <Header 
                    title='Recarga'
                />
                <Content>
                    <Text style={styles.txtTopic}>SELECIONE CONTA DE ORIGEM</Text>
                    <Line />
                    <AccountItem 
                      data={this.listAccounts()}
                    />
                    {opSelected !== '' &&
                        <Text style={{ alignSelf: 'center', marginBottom: 16, color: colors.lightGray, fontSize: 18 }}>
                            {opSelected}
                        </Text>
                    }
                    <ModalItem
                        handleModal={() => this.setState({ modal: true })}
                        closeModal={() => {}}
                        modalVisible={modal}
                        pressSelect={this.setSelected}
                        data={this.listOp()}
                        dataSeached={searchedList}
                        txtSelected={txtSearch}
                        searchItem={this._searchItem}
                        
                    />
                    <Spacer height={20} />
                    <Text style={styles.txtTopic}>NÚMERO DE TELEFONE</Text>
                    <Line />
                    <View style={{ flexDirection: 'row' }}>
                        <Input
                            long={20}
                            value={ddd}
                            editable
                            autoCorrect={false}
                            onChangeText={value => this.setState({ ddd : value })}
                            multiline={false}
                            underlineColorAndroid='transparent'
                            placeholder='DDD'
                            keyboardType='numeric'
                            maxLength={2}
                            search={this.state.focusddd}
                            onBlur={() => this.setState({ focusddd: false })}
                            onFocus={() => this.setState({ focusddd: true })}
                        />
                        <Input 
                            long={40}
                            value={number}
                            editable
                            autoCorrect={false}
                            onChangeText={value => this.setState({ number : value })}
                            multiline={false}
                            underlineColorAndroid='transparent'
                            keyboardType='numeric'
                            maxLength={9}
                            search={this.state.fnumber}
                            onBlur={() => this.setState({ fnumber: false })}
                            onFocus={() => this.setState({ fnumber: true })}
                        />
                        <TouchableOpacity style={{ alignSelf: 'flex-end' }} onPress={() => this.getPhoneNumber()}>
                            <Image style={{ width:30, height: 30 }} source={icons['user']} />
                        </TouchableOpacity>
                    </View>
                    <Spacer height={10} />
                    <Text style={{ color: colors.lightGray }}>Repita o número por seguraça</Text>
                    <View style={{ flexDirection: 'row' }}>
                        <Input
                            long={20}
                            value={dddConfirm}
                            editable
                            autoCorrect={false}
                            onChangeText={value => this.setState({ dddConfirm : value })}
                            multiline={false}
                            underlineColorAndroid='transparent'
                            placeholder='DDD'
                            keyboardType='numeric'
                            maxLength={2}
                            search={this.state.fcddd}
                            onBlur={() => this.setState({ fcddd: false })}
                            onFocus={() => this.setState({ fcddd: true })}
                        />
                        <Input 
                            long={40}
                            value={numberConfirm}
                            editable
                            autoCorrect={false}
                            onChangeText={value => this.setState({ numberConfirm : value })}
                            multiline={false}
                            underlineColorAndroid='transparent'
                            keyboardType='numeric'
                            maxLength={9}
                            search={this.state.fcnumber}
                            onBlur={() => this.setState({ fcnumber: false })}
                            onFocus={() => this.setState({ fcnumber: true })}
                        />
                    </View>
                    <Spacer height={20} />
                    <Text style={styles.txtTopic}>VALOR</Text>
                    <Line />
                    <ValuesItem 
                        select='value' 
                        data={this.listValues()}
                        pressSelect={this.setValue}
                    />
                    <Spacer height={20} />
                    <Text style={styles.txtTopic}>DATA DE RECARGA</Text>
                    <Line />
                    <DateItem
                        data={this.listDate()}
                        pressSelectDate={() => this.setDate}
                    />
                    <Spacer height={20} />
                    <CheckBox
                        press={() => this.setState({ saveRec: !this.state.saveRec }) }
                        value={this.state.saveRec}
                        label='Guardar recarga'
                    />
                    <Spacer height={20} />
                    <ButtonItem 
                        press={() => this.handleRequest()}
                        title='Continuar'
                        widthValue={50}
                    />
                    <DateTimePicker
                        mode='date'
                        isVisible={this.state.pickerVisible}
                        onConfirm={this._handleDatePicked}
                        onCancel={this._hideDateTimePicker}
                    />
                </Content>
            </Container>
        );
    }
}

const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  txtSelected: {
    fontWeight: 'bold', 
    marginBottom: 12, 
    color: colors.gray, 
    alignSelf: 'center'
  },
  txtTopic: {
    marginTop: 10,
    marginBottom: 5,
    color: colors.red
  }
};
