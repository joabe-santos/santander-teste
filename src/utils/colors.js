const colors = {
    brown1: '#d9b38c',
    white: '#fff',
    black: '#000',
    red: '#cc0000',
    darkGray: '#444444',
    lightGray: '#cccccc',
    mediuGray: '#767676'
};

export default colors;

