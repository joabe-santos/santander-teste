const icons = {
    left: require('../assets/left.png'),
    right: require('../assets/right.png'),
    ckeck: require('../assets/tick.png'),
    user: require('../assets/user.png')
}

export default icons;
