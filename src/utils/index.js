import colors from './colors';
import icons from './icons';

const months = value => {
    let mes;
    switch (value) {
        case 'Jan':
            mes = '01'
        break
        case 'Feb':
            mes = '02'
        break
        case 'Mar':
            mes = '03'
        break
        case 'Apr':
            mes = '04'
        break
        case 'May':
            mes = '05'
        break
        case 'Jun':
            mes = '06'
        break
        case 'Jul':
            mes = '07'
        break
        case 'Aug':
            mes = '08'
        break
        case 'Sep':
            mes = '09'
        break
        case 'Oct':
            mes = '10'
        break
        case 'Nov':
            mes = '11'
        break
        case 'Dec':
            mes = '12'
        break
            default: ''
    }
    return mes
};

export {
    colors,
    icons,
    months
};
